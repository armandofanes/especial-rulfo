/*miguel*/
var irSeccion = (seccion) => {
    $('html,body').animate({
        scrollTop: $("#" + seccion).offset().top - $('header.page-header').height()
    }, 'slow');
}

var animarNombres = () => {

    var nombres = document.querySelectorAll('p.nombres span');

    var contadorTiempo = 1;
    var tiempoAparicion = 500;
    var contadorElementos = 0;
    [].forEach.call(nombres, (n) => {


        setTimeout(() => {

            n.classList.add('fadeIn');
        }, tiempoAparicion * contadorTiempo);

        if (contadorElementos == 0 || contadorElementos == 4) {
            setTimeout(() => {
                n.classList.add('remark');
            }, tiempoAparicion * (nombres.length + 1.5));
            setTimeout(() => {
                document.querySelector('#videoNombresRulfo').classList.add('fadeIn');
            }, tiempoAparicion * (nombres.length + 1.5));
        }

        contadorTiempo++;
        contadorElementos++;

    });

}


var buscarSeccionActiva = () => {
    var secciones = document.querySelectorAll('section');
    var seccionActiva = '';
    [].forEach.call(secciones, (s) => {

        if (window.scrollY + 1 > (s.offsetTop - (window.scrollY / 4))) {
            seccionActiva = s.getAttribute('id');
        }
    });
    $('.menu-lat .seccion').removeClass('activo');
    if (seccionActiva != '') {
        document.querySelector(`.menu-lat .seccion[data-activar="${seccionActiva}"]`).classList.add('activo');
    }
}
/*miguel*/



$(document).ready(() => {
    var alto = $(window).height();
    $('#section-01').css({
        'height': '' + alto + 'px'
    });
});


var init = () => {

    var loaderInicio = document.querySelector('.loader-inicio');
    loaderInicio.classList.add('fadeOut');
    loaderInicio.addEventListener('animationend', () => {
      document.body.classList.remove('no-overflow');
      document.querySelector('body').removeChild(loaderInicio);



        /*miguel*/
        var animacionNombresActiva = false;

        var secciones = document.querySelectorAll('section');



        var contenidoDots = '';
        [].forEach.call(secciones, (s) => {
            contenidoDots = `${contenidoDots}<li class="seccion" data-activar="${s.getAttribute('id')}" onclick="irSeccion('${s.getAttribute('id')}')"><a class="btn-m"></a></li>`
        });

        document.querySelector('.menu-lat').innerHTML = contenidoDots;

        var audioRep = document.querySelectorAll('.audioRep');



        var pararReproductores = () => {
            [].forEach.call(audioRep, (a) => {
                var botonRep = a.querySelector('.play-pause');
                var reproductor = a.querySelector('.repR audio');
                var botonRep = a.querySelector('.play-pause');
                botonRep.classList.remove('pause');
                botonRep.classList.add('play');
                reproductor.pause();
            });
        }

        [].forEach.call(audioRep, (a) => {

            var botonRep = a.querySelector('.play-pause');
            var reproductor = a.querySelector('.repR audio');
            var barraClickable = a.querySelector('.repV .barra');
            var barraReproductor = a.querySelector('.repV .barra .bolita');

            botonRep.addEventListener('click', (e) => {
                e.preventDefault();

                if (reproductor.paused) {
                    pararReproductores();
                    botonRep.classList.remove('play');
                    botonRep.classList.add('pause');

                    reproductor.play();
                } else {
                    botonRep.classList.remove('pause');
                    botonRep.classList.add('play');
                    reproductor.pause();
                }

            });


            barraClickable.addEventListener('click',(e) => {
              //obtenemos el ancho de la barra
              var ancho = barraClickable.offsetWidth;
              //obtenemos donde se dió click
              var offset = $(barraClickable).offset();
              var relativeX = (e.pageX - offset.left);
              var porcentajeBarra = (100*relativeX)/ancho;
              //mandamos el audio a esa posición
              var totalPista = reproductor.duration;
              var pistaA = porcentajeBarra * totalPista / 100;
              reproductor.currentTime = pistaA;
            });

            reproductor.addEventListener('ended',() => {
              reproductor.currentTime = 0;
              botonRep.classList.remove('pause');
              botonRep.classList.add('play');
            });


            buscarSeccionActiva();

            window.addEventListener('scroll', () => {

                var barraSuperior = document.querySelector('header.page-header');
                if (window.scrollY > barraSuperior.offsetTop) {
                    barraSuperior.classList.add('active');
                } else {
                    barraSuperior.classList.remove('active');
                }


                buscarSeccionActiva();


                var pNombres = document.querySelector('#section-04');

                if (window.scrollY > (pNombres.offsetTop - (window.scrollY / 2))) {
                    if (!animacionNombresActiva) {
                        animarNombres();
                    }
                    animacionNombresActiva = true;
                }

                $('#listaRedes').slideUp();
                redesActivas = false;

            }); //scroll


            /*miguel*/

            var redesActivas = false;
            $('#botonVerRedes').click((e) => {
                e.preventDefault();
                if (!redesActivas) {
                    $('#listaRedes').slideDown();
                } else {
                    $('#listaRedes').slideUp();
                }
                redesActivas = !redesActivas;
            });


            var ventana = $(window).width();




            function scrollToAnchor(aid) {
                var aTag = $("a[name='" + aid + "']");
                $('html,body').animate({
                    scrollTop: aTag.offset().top - 100
                }, 'slow');
            }
            $('#anchor-slide').click(function(e) {

                e.preventDefault();
                scrollToAnchor($(this).attr('data-destino'));
            });

            //Alto dinamico del index
            var alto = $(window).height();
            $('#section-01').css({
                'height': '' + alto + 'px'
            });
            $(window).on('resize', function() {
                var alto = $(window).height();
                $('#section-01').css({
                    'height': alto + 'px'
                });

            });
            //Fin de alto dinamico del index
            var winHeight = $(window).height();
            var docHeight = $(document).height();
            var max = docHeight - winHeight;

            $('#progreso').attr('max', max);
            var value = $(window).scrollTop();

            $('#progreso').attr('value', value);
            $(document).on('scroll', function() {
                value = $(window).scrollTop();
                $('#progreso').attr('value', value);
            });

            function scrollToAnchor(aid) {
                var aTag = $("a[name='" + aid + "']");
                $('html,body').animate({
                    scrollTop: aTag.offset().top - 100
                }, 'slow');
            }
            $('#anchor-slide').click(function(e) {

                e.preventDefault();
                scrollToAnchor($(this).attr('data-destino'));
            });

            //Alto dinamico del index
            var alto = $(window).height();
            $('#section-01').css({
                'height': '' + alto + 'px'
            });
            $(window).on('resize', function() {
                var alto = $(window).height();
                $('#section-01').css({
                    'height': alto + 'px'
                });

            });

            reproductor.addEventListener('timeupdate', (el) => {
                //se actualiza la barra de progreso

                var avance = (reproductor.currentTime * 100) / reproductor.duration;

                barraReproductor.style.width = avance + '%';
            });

            //audio.duration

        });

        buscarSeccionActiva();

        window.addEventListener('scroll', () => {

            var barraSuperior = document.querySelector('header.page-header');
            if (window.scrollY > barraSuperior.offsetTop) {
                barraSuperior.classList.add('active');
            } else {
                barraSuperior.classList.remove('active');
            }


            buscarSeccionActiva();


            var pNombres = document.querySelector('#section-04');

            if (window.scrollY > (pNombres.offsetTop - (window.scrollY / 2))) {
                if (!animacionNombresActiva) {
                    animarNombres();
                }
                animacionNombresActiva = true;
            }

            $('#listaRedes').slideUp();
            redesActivas = false;

        }); //scroll



        /*miguel*/

        var redesActivas = false;
        $('#botonVerRedes').click((e) => {
            e.preventDefault();
            if (!redesActivas) {
                $('#listaRedes').slideDown();
            } else {
                $('#listaRedes').slideUp();
            }
            redesActivas = !redesActivas;
        });


        var fondoIntro = document.querySelector('.fondo-intro');
        fondoIntro.classList.add('fondo-intro-normal');
        setTimeout(() => {
            document.querySelector('#section-01 .fondo-intro .rostro-rulfo').classList.add('fadeIn');
        }, 3000);

        function scrollToAnchor(aid) {
            var aTag = $("a[name='" + aid + "']");
            $('html,body').animate({
                scrollTop: aTag.offset().top - 100
            }, 'slow');
        }
        $('#anchor-slide').click(function(e) {

            e.preventDefault();
            scrollToAnchor($(this).attr('data-destino'));
        });

        //Alto dinamico del index

        $(window).on('resize', function() {
            var alto = $(window).height();
            $('#section-01').css({
                'height': alto + 'px'
            });

        });
        //Fin de alto dinamico del index
        var winHeight = $(window).height();
        var docHeight = $(document).height();
        var max = docHeight - winHeight;

        $('#progreso').attr('max', max);
        var value = $(window).scrollTop();

        $('#progreso').attr('value', value);
        $(document).on('scroll', function() {
            value = $(window).scrollTop();
            $('#progreso').attr('value', value);
        });

        function scrollToAnchor(aid) {
            var aTag = $("a[name='" + aid + "']");
            $('html,body').animate({
                scrollTop: aTag.offset().top - 100
            }, 'slow');
        }
        $('#anchor-slide').click(function(e) {

            e.preventDefault();
            scrollToAnchor($(this).attr('data-destino'));
        });

        //Alto dinamico del index
        var alto = $(window).height();
        $('#section-01').css({
            'height': '' + alto + 'px'
        });
        $(window).on('resize', function() {
            var alto = $(window).height();
            $('#section-01').css({
                'height': alto + 'px'
            });

        });


    });//fin de loader
}


window.onload = () => {
    setTimeout(() => {
        init();
    }, 0);
}
